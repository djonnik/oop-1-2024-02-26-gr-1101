# Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
# назву, рік видання та жанрі. Створіть кілька книжок. Визначте для нього
# операції перевірки на рівність та нерівність, методи __repr__ та __str__.


class Author:
    def __init__(self, first_name: str, last_name: str):
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Genre:
    def __init__(self, title: str, description: str):
        self.title = title
        self.description = description

    def __str__(self):
        return f"{self.title}"


class Book:
    def __init__(
        self,
        title: str,
        year_of_publisher: int,
        authors: list[Author],
        genres: list[Genre],
    ):
        self.title = title
        self.year_of_publisher = year_of_publisher
        self.authors = authors
        self.genres = genres


if __name__ == "__main__":
    ...
